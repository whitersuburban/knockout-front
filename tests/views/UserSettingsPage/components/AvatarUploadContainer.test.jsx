/* eslint-disable no-unused-vars */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import AvatarUploadContainer from '../../../../src/views/UserSettingsPage/components/AvatarUploadContainer';
import { customRender, screen } from '../../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';
import { BASIC_USER, GOLD_USER } from '../../../../src/utils/roleCodes';

describe('Avatar Upload Container Component', () => {
  beforeEach(() => {
    localStorage.clear();
  });
  describe('as a logged in blue member', () => {
    const loggedInState = {
      user: {
        loggedIn: true,
        username: 'TestUser',
        avatarUrl: 'avatar.png',
      },
    };

    const userLocalStorageDetails = {
      id: 123,
      username: 'TestUser',
      role: { code: BASIC_USER },
      avatarUrl: 'avatar.png',
    };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
    });

    it('displays you-must-be-gold-member info for blue members', () => {
      customRender(<AvatarUploadContainer currentUser={userLocalStorageDetails} />, {
        initialState: loggedInState,
      });

      expect(
        screen.queryByText('You must be a Gold member to upload animated avatars.')
      ).toBeDefined();
    });
  });
  describe('as a logged in gold member', () => {
    const loggedInState = {
      user: {
        loggedIn: true,
        username: 'GoldenUser',
        avatarUrl: 'avatar.png',
      },
    };

    const userLocalStorageDetails = {
      id: 234,
      username: 'GoldenUser',
      role: { code: GOLD_USER },
      avatarUrl: 'avatar.png',
    };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
    });

    it('does not display you-must-be-gold-member info for gold members', () => {
      customRender(<AvatarUploadContainer />, {
        initialState: loggedInState,
      });

      expect(
        screen.queryByText('You must be a Gold member to upload animated avatars.')
      ).toBeNull();
    });
  });
});
