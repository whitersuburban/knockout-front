import React from 'react';
import { customRender } from '../../../custom_renderer';
import PageSwitcher from '../../../../src/views/UserSetupPage/components/PageSwitcher';
import '@testing-library/jest-dom/extend-expect';

describe('The Page Switcher Component', () => {
  describe('is on the first page', () => {
    it('will display a next button', () => {
      const { getByText } = customRender(
        <PageSwitcher pageAmount={3} currentPage={1} nextPage={() => {}} prevPage={() => {}} />
      );
      expect(getByText('Next'));
    });
    it('will not display a back button', () => {
      const { queryByText } = customRender(
        <PageSwitcher pageAmount={3} currentPage={1} nextPage={() => {}} prevPage={() => {}} />
      );
      expect(queryByText('Back')).toBeNull();
    });
  });

  describe('is on a middle page', () => {
    it('will display a next button', () => {
      const { getByText } = customRender(
        <PageSwitcher pageAmount={3} currentPage={2} nextPage={() => {}} prevPage={() => {}} />
      );
      expect(getByText('Next'));
    });
    it('will display a back button', () => {
      const { getByText } = customRender(
        <PageSwitcher pageAmount={3} currentPage={2} nextPage={() => {}} prevPage={() => {}} />
      );
      expect(getByText('Back'));
    });
  });

  describe('is on the last page', () => {
    it('will not display a next button', () => {
      const { queryByText } = customRender(
        <PageSwitcher pageAmount={3} currentPage={3} nextPage={() => {}} prevPage={() => {}} />
      );
      expect(queryByText('Next')).toBeNull();
    });
    it('will display a back button', () => {
      const { getByText } = customRender(
        <PageSwitcher pageAmount={3} currentPage={3} nextPage={() => {}} prevPage={() => {}} />
      );
      expect(getByText('Back'));
    });
  });
});
