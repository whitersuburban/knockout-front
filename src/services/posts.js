import axios from 'axios';
import config from '../../config';
import { authGet, authPost, authPut } from './common';
import { POST_CHARACTER_LIMIT } from '../utils/limits';

const APP_NAME = 'knockout.chat';

const validateContent = (content) => {
  const contentSerialized = content.trim();
  const contentLength = contentSerialized.length;

  if (contentLength < 1) {
    throw new Error({ error: 'Post body too short.' });
  }

  const lineBreakAdjustment = (contentSerialized.match(/\n/g) || '').length * 65;

  if (contentLength + lineBreakAdjustment > POST_CHARACTER_LIMIT) {
    throw new Error({ error: `Posts must be under ${POST_CHARACTER_LIMIT} characters.` });
  }

  return content.trim();
};

export const submitPost = ({ content, thread_id: threadId, sendCountryInfo }) => {
  const contentStringified = validateContent(content);

  const requestBody = {
    content: contentStringified,
    thread_id: threadId,
    display_country: sendCountryInfo,
    app_name: APP_NAME,
  };

  return authPost({ url: '/v2/posts', data: requestBody });
};

export const getPostList = async () => {
  const res = await axios.get(`${config.apiHost}/post`);

  const { data } = res;

  return data;
};

export const getPost = (postId) => authGet({ url: `/v2/posts/${postId}` });

export const updatePost = ({ content, id }) => {
  const contentStringified = validateContent(content);

  const requestBody = {
    content: contentStringified,
    app_name: APP_NAME,
  };

  return authPut({ url: `/v2/posts/${id}`, data: requestBody });
};
