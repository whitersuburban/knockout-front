import dayjs from 'dayjs';
import { useContext, useEffect, useState } from 'react';
import PostContext from '../../Post/PostContext';
import {
  loadApprovedUserIdsFromStorage,
  setApprovedUserIdsToStorage,
} from '../../../services/theme';

const isLimitedUser = (user, postEdited, isUserApproved) => {
  const newUser = dayjs(user.createdAt).add(2, 'week').isAfter(dayjs());
  const lowPostCount = user.posts < 15;
  return (
    (newUser || lowPostCount) &&
    dayjs(postEdited).add(18, 'hour').isAfter(dayjs()) &&
    !isUserApproved
  );
};

export default (spoiler) => {
  const { user, postEdited } = useContext(PostContext);
  const [approvedUserIds, setApprovedUserIds] = useState(new Set());
  const limitedUser = isLimitedUser(user, postEdited, approvedUserIds.has(user.id));
  const [spoilered, setSpoilered] = useState(spoiler || limitedUser);

  useEffect(() => {
    setApprovedUserIds(new Set(loadApprovedUserIdsFromStorage()));
  }, []);

  useEffect(() => {
    setSpoilered(spoiler || limitedUser);
  }, [user, limitedUser, spoiler]);

  useEffect(() => {
    if (!spoilered && limitedUser) {
      // user has unspoilerd a media element for the limited user that wasn't spoilered otherwise,
      // so remember the users ID, that way we don't set this element as spoilered for the user ID again
      setApprovedUserIdsToStorage([...approvedUserIds, user.id]);
    }
  }, [spoilered, limitedUser, approvedUserIds, user.id]);

  const updateApprovedUserIds = () => {
    setApprovedUserIds(new Set(loadApprovedUserIdsFromStorage()));
  };

  useEffect(() => {
    window.addEventListener('approvedUserIds', updateApprovedUserIds);

    return () => {
      window.removeEventListener('approvedUserIds', updateApprovedUserIds);
    };
  }, []);

  const reveal = (setModalOpen) => {
    if (limitedUser) {
      setModalOpen(true);
    } else {
      setSpoilered(false);
    }
  };

  return [spoilered, setSpoilered, reveal, limitedUser];
};
