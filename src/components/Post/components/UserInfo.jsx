/* eslint-disable react/display-name */
import React, { forwardRef, useState } from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import styled from 'styled-components';

import { Link } from 'react-router-dom';
import config from '../../../../config';
import UserRoleWrapper from '../../UserRoleWrapper';
import {
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeHighlightWeaker,
} from '../../../utils/ThemeNew';
import { formattedUsername } from '../../../utils/user';
import { isDeletedUser } from '../../../utils/deletedUser';
import userRoles from '../../UserRoleWrapper/userRoles';
import { roleCheck } from '../../UserRoleRestricted';
import { REGULAR_USER_ROLES } from '../../../utils/roleCodes';
import GoldCheck from '../../GoldCheck';

// Returns a title component for a user,
// or null otherwise.
const titleFromUser = (user) => {
  let titleString = null;

  if (user?.title) {
    titleString = user.title;
  } else if (userRoles[user.role?.code]?.title) {
    titleString = userRoles[user.role.code].title;
  }

  if (!titleString) {
    return null;
  }

  return (
    <div className="user-title" title={`${user.username}'s title`}>
      {titleString}
    </div>
  );
};

const UserInfoWrapper = styled.div`
  display: flex;
  justify-content: start;
  align-items: center;
  flex-direction: column;
  position: relative;
  overflow: hidden;
  ${(props) => (!props.$profileView ? 'min-height: 120px;' : 'min-height: 440px;')}
  background: rgba(255, 255, 255, 0.05);
  padding-top: 37px;

  transition: min-height 250ms ease-in-out;

  @media (min-width: 900px) {
    &:hover {
      .user-background {
        filter: none;
      }
    }
    ${(props) =>
      props.viewBackground &&
      `
      min-height: 440px;

      .user-background {
        filter: none;
      }

      .user-avatar,
      .user-join-date,
      .user-role-wrapper-component,
      .user-title,
      .user-pronouns
      {
        opacity: 0;
      }`}

    .user-role-wrapper-component {
      order: -2;
    }

    .user-pronouns {
      order: -1;
    }

    .user-title {
      order: 2;
    }
  }

  .profile-link {
    z-index: 2;
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .user-info {
    display: contents;
    margin-bottom: ${ThemeVerticalPadding};

    .user-title,
    .user-pronouns,
    .user-join-date {
      z-index: 2;
      font-size: ${ThemeFontSizeSmall};
      transition: opacity ease-in-out 500ms;
    }

    .user-join-date {
      ${(props) => !props.hasAvatar && `margin-top: ${ThemeVerticalPadding(props)};`}
    }

    .user-info-name {
      line-height: 1.3em;
      ${(props) =>
        props.$hasBackground && props.regularUser && 'filter: drop-shadow(0px 0px 2px black);'}
    }

    .online {
      border-bottom: 2px dotted ${ThemeHighlightWeaker};
    }

    @media (max-width: 800px) {
      display: block;
      margin: auto;

      .user-title,
      .user-pronouns {
        margin: calc(${ThemeVerticalPadding} / 2) 0 calc(${ThemeVerticalPadding} / 2) 0;
      }
    }
  }

  .user-title,
  .user-pronouns {
    z-index: 2;
    margin-top: calc(${ThemeVerticalPadding} / 2);
    font-size: ${ThemeFontSizeSmall};
    transition: opacity ease-in-out 500ms;
  }

  .user-pronouns {
    margin-bottom: 0;
  }

  .user-title {
    margin-bottom: calc(${ThemeVerticalPadding} / 2);
  }

  .user-role-wrapper-component {
    font-size: ${ThemeFontSizeMedium};
    z-index: 3;
    top: 175px;
    left: 50%;
    font-weight: bold;
    transition: opacity ease-in-out 500ms;
  }

  .user-join-date,
  .user-title,
  .user-pronouns {
    font-size: ${ThemeFontSizeSmall};
    z-index: 3;
    top: 195px;
    left: 50%;
    ${(props) => props.$hasBackground && 'color: white;'}
    transition: opacity ease-in-out 500ms;
    filter: drop-shadow(0px 0px 5px black);
  }

  .deleted-user {
    pointer-events: none;
  }

  .user-avatar {
    z-index: 2;
    margin: ${ThemeVerticalPadding} 0 ${ThemeVerticalPadding} 0;
    ${(props) => props.viewBackground && 'opacity: 0;'};
    transition: opacity ease-in-out 500ms;
  }

  .user-background {
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    content: '';
    filter: ${(props) => (props.viewBackground ? 'none' : 'brightness(0.5) contrast(0.925)')};
    transition: filter ease-in-out 500ms;
    cursor: pointer;
    user-select: none;
  }

  .user-background-image {
    mask-image: linear-gradient(rgb(255, 255, 255) 0%, rgb(255, 255, 255) 85%, transparent 100%);
  }

  @media (max-width: 900px) {
    overflow: hidden;
    min-height: unset;
    height: 5rem;
    align-items: left;
    justify-content: left;
    padding: unset;
    flex-direction: row;
    padding-left: ${ThemeHorizontalPadding};

    .user-role-wrapper-component {
      position: initial;
      transform: none;
    }
    .profile-link {
      align-items: left;
      justify-content: left;
      flex-direction: row;
    }
    .user-avatar {
      position: initial;
      width: auto;
      transform: none;
      max-width: 70px;
      max-height: 70px;
      margin: 0 ${ThemeHorizontalPadding} 0 0;
    }
    .user-join-date {
      display: none;
    }

    .user-background-image {
      position: absolute;
      width: 100%;
      top: 50%;
      transform: translateY(-50%);
    }

    .user-info {
      display: block;
      z-index: 2;

      .user-pronouns {
        padding-top: 0;
      }
    }
  }
`;

const UserInfo = forwardRef(({ user, profileView }, ref) => {
  const [viewBackground, setViewBackground] = useState(false);

  const hasAvatar =
    user.avatarUrl && user.avatarUrl.length !== 0 && !user.avatarUrl.includes('none.webp');
  let url = `${config.cdnHost}/image/${user.avatarUrl}`;
  let title = `${user.username}'s avatar`;
  const bgUrl = user.backgroundUrl ? `${config.cdnHost}/image/${user.backgroundUrl}` : undefined;
  const userJoinDateShort = dayjs(user.createdAt).format('MMM YYYY');
  const userJoinDateLong = new Intl.DateTimeFormat(undefined, {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
  }).format(new Date(user.createdAt));
  const userCakeDay = dayjs(user.createdAt).format('DD/MM') === dayjs(new Date()).format('DD/MM');

  if (!hasAvatar) {
    url = `${config.cdnHost}/image/none.webp`;
  }
  if (user.isBanned) {
    url = 'https://img.icons8.com/color/80/000000/minus.png';
    title = `${user.username} is muted!`;
  }

  const deletedUser = isDeletedUser(user.username);

  let linkTo = `/user/${user.id}`;
  if (deletedUser) {
    linkTo = '#';
  }

  const bgTitleText = `${user.username}'s Background Avatar`;

  return (
    <UserInfoWrapper
      $profileView={profileView}
      $hasBackground={bgUrl !== undefined}
      regularUser={roleCheck(REGULAR_USER_ROLES)}
      hasAvatar={hasAvatar}
      title={user.title}
      viewBackground={viewBackground}
      className={`user-wrapper${deletedUser ? ' deleted-user' : ''}`}
    >
      {deletedUser && (
        <div className="user-info">
          <UserRoleWrapper user={user} className={bgUrl && 'user-info-name'}>
            {formattedUsername(user.username)}
          </UserRoleWrapper>
        </div>
      )}
      {!deletedUser && (
        <Link className="profile-link" title={`${user.username}'s profile`} to={linkTo} ref={ref}>
          {hasAvatar && <img className="user-avatar" alt={title} src={url} />}
          <div className="user-info">
            <span className="user-role-wrapper-component">
              <UserRoleWrapper
                user={user}
                className={`${user.online ? 'online' : ''}${bgUrl ? ' user-info-name' : ''}`}
              >
                {formattedUsername(user.username)}
              </UserRoleWrapper>
              <GoldCheck user={user} margin={3} />
            </span>

            {titleFromUser(user)}
            {user.pronouns && (
              <div className="user-pronouns" title="Pronouns">
                {user.pronouns}
              </div>
            )}
            {!deletedUser && (
              <span className="user-join-date" title={userJoinDateLong}>
                {userCakeDay && '🍰 '}
                {userJoinDateShort}
                {userCakeDay && ' 🎉'}
              </span>
            )}
          </div>
        </Link>
      )}
      {!deletedUser && bgUrl && (
        <div
          className="user-background"
          onClick={() => setViewBackground((value) => !value)}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              setViewBackground((value) => !value);
            }
          }}
          role="button"
          tabIndex="0"
          title={bgTitleText}
        >
          <img
            className="user-background-image"
            alt=""
            title={bgTitleText}
            draggable="false"
            onDragStart={(e) => {
              e.preventDefault();
            }}
            src={bgUrl}
          />
        </div>
      )}
    </UserInfoWrapper>
  );
});

UserInfo.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    title: PropTypes.string,
    pronouns: PropTypes.string,
    backgroundUrl: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    isBanned: PropTypes.bool.isRequired,
    online: PropTypes.bool,
  }).isRequired,
  profileView: PropTypes.bool,
};

UserInfo.defaultProps = {
  profileView: false,
};

export default UserInfo;
