import { User } from 'knockout-schema';
import React from 'react';
import styled from 'styled-components';
import { GOLD_USER_ROLES } from '../../utils/roleCodes';
import { aprilFools } from '../../utils/eventDates';

interface GoldCheckProps {
  user: User;
  margin?: number;
}

interface StyledGoldCheckProps {
  margin: number;
}

const StyledGoldCheck = styled.i<StyledGoldCheckProps>`
  margin-left: ${(props) => props.margin}px;
  color: #1d9bf0;
`;

const GoldCheck = ({ user, margin = 5 }: GoldCheckProps) =>
  aprilFools() &&
  GOLD_USER_ROLES.includes(user?.role?.code) && (
    <StyledGoldCheck margin={margin} className="fa-solid fa-circle-check" />
  );

export default GoldCheck;
