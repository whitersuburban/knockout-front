/* eslint-disable no-alert */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-param-reassign */
import config from '../../../config';
import replaceBetween from '../../utils/replaceBetween';
import getImageFromClipboard from '../../utils/getImageFromClipboard';
import { pushSmartNotification } from '../../utils/notification';
import {
  isBoldHotkey,
  isCodeHotkey,
  isItalicHotkey,
  isStrikethroughHotkey,
  isSubmitHotkey,
  isUnderlinedHotkey,
} from './editorHotkeys';

import { isImage } from '../KnockoutBB/components/ImageBB';
import { getStreamableId } from '../KnockoutBB/components/StreamableBB';
import { getTweetId } from '../KnockoutBB/components/TweetBB';
import { isVideo } from '../KnockoutBB/components/VideoBB';
import { getVimeoId } from '../KnockoutBB/components/VimeoBB';
import { getYoutubeId } from '../KnockoutBB/components/YoutubeBB';
import { getVocarooId } from '../KnockoutBB/components/VocarooBB';
import { getSpotifyId } from '../KnockoutBB/components/SpotifyBB';
import { getTwitchId } from '../KnockoutBB/components/TwitchBB';
import { getSoundCloudId } from '../KnockoutBB/components/SoundCloudBB';
import { isReddit } from '../KnockoutBB/components/RedditBB';
import { isInstagram } from '../KnockoutBB/components/InstagramBB';
import { getTikTokId } from '../KnockoutBB/components/TikTokBB';
import { isMastodon } from '../KnockoutBB/components/MastodonBB';

import uploadImage from '../../services/images';
import { getTumblrId } from '../KnockoutBB/components/TumblrBB';

const URL_TAG = 'url';

export const httpRegex = new RegExp(
  '^' +
    '(?:(?:(?:https?|ftp):)?\\/\\/)' +
    '(?:\\S+(?::\\S*)?@)?' +
    '(?:' +
    '(?!(?:10|127)(?:\\.\\d{1,3}){3})' +
    '(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})' +
    '(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})' +
    '(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])' +
    '(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}' +
    '(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))' +
    '|' +
    '(?:' +
    '(?:' +
    '[a-z0-9\\u00a1-\\uffff]' +
    '[a-z0-9\\u00a1-\\uffff_-]{0,62}' +
    ')?' +
    '[a-z0-9\\u00a1-\\uffff]\\.' +
    ')+' +
    '(?:[a-z\\u00a1-\\uffff]{2,}\\.?)' +
    ')' +
    '(?::\\d{2,5})?' +
    '(?:[/?#]\\S*)?' +
    '$',
  'i'
);

function replaceWithin(input, start, end, replacement) {
  return `${input.substring(0, start)}${replacement}${input.substring(end)}`;
}

function modifySelectionRange(input, cursorPosition) {
  setTimeout(() => {
    input.setSelectionRange(cursorPosition, cursorPosition);
    input.focus();
  }, 100);
}

export const handlePaste = async (e, setContent, selectionRange) => {
  const clipboardData = e.clipboardData.getData('Text');
  let content;
  let isDefaultHttpUrl = false;

  const clipboardImage = getImageFromClipboard(e.clipboardData);

  if (clipboardImage) {
    if (confirm('Upload image from clipboard?')) {
      e.preventDefault();
      const response = await uploadImage({ imageBlob: clipboardImage });
      if (response && response.data && response.data.fileName) {
        pushSmartNotification({ message: 'Image uploaded successfully!' });
        const imageHttpUrl = `${config.cdnHost}/image/${response.data.fileName}`;
        content = `[img]${imageHttpUrl}[/img]`;
      } else {
        pushSmartNotification({ error: `Failed to upload image: ${response.data.message}` });
      }
    }
  } else if (isImage(clipboardData)) {
    if (confirm('Insert image?')) {
      e.preventDefault();
      content = `[img]${clipboardData}[/img]`;
    }
  } else if (getYoutubeId(clipboardData)) {
    if (confirm('Insert Youtube?')) {
      e.preventDefault();
      content = `[youtube]${clipboardData}[/youtube]`;
    }
  } else if (getVimeoId(clipboardData)) {
    if (confirm('Insert Vimeo video?')) {
      e.preventDefault();
      content = `[vimeo]${clipboardData}[/vimeo]`;
    }
  } else if (getStreamableId(clipboardData)) {
    if (confirm('Insert Streamable video?')) {
      e.preventDefault();
      content = `[streamable]${clipboardData}[/streamable]`;
    }
  } else if (getVocarooId(clipboardData)) {
    if (confirm('Insert Vocaroo?')) {
      e.preventDefault();
      setContent((prevState) => `${prevState}[vocaroo]${clipboardData}[/vocaroo]`);
    }
  } else if (isVideo(clipboardData)) {
    if (confirm('Insert video?')) {
      e.preventDefault();
      content = `[video]${clipboardData}[/video]`;
    }
  } else if (getTweetId(clipboardData)) {
    if (confirm('Insert twitter?')) {
      e.preventDefault();
      content = `[twitter]${clipboardData}[/twitter]`;
    }
  } else if (getSpotifyId(clipboardData)) {
    if (confirm('Insert Spotify?')) {
      e.preventDefault();
      content = `[spotify]${clipboardData}[/spotify]`;
    }
  } else if (getTwitchId(clipboardData)) {
    if (confirm('Insert Twitch?')) {
      e.preventDefault();
      content = `[twitch]${clipboardData}[/twitch]`;
    }
  } else if (getSoundCloudId(clipboardData)) {
    if (confirm('Insert SoundCloud?')) {
      e.preventDefault();
      content = `[soundcloud]${clipboardData}[/soundcloud]`;
    }
  } else if (isReddit(clipboardData)) {
    if (confirm('Insert Reddit?')) {
      e.preventDefault();
      content = `[reddit]${clipboardData}[/reddit]`;
    }
  } else if (isInstagram(clipboardData)) {
    if (confirm('Insert Instagram?')) {
      e.preventDefault();
      content = `[instagram]${clipboardData}[/instagram]`;
    }
  } else if (getTikTokId(clipboardData)) {
    if (confirm('Insert TikTok?')) {
      e.preventDefault();
      content = `[tiktok]${clipboardData}[/tiktok]`;
    }
  } else if (getTumblrId(clipboardData)) {
    if (confirm('Insert Tumblr?')) {
      e.preventDefault();
      content = `[tumblr]${clipboardData}[/tumblr]`;
    }
  } else if (isMastodon(clipboardData)) {
    if (confirm('Insert Mastodon?')) {
      e.preventDefault();
      content = `[mastodon]${clipboardData}[/mastodon]`;
    }
  } else if (httpRegex.test(clipboardData)) {
    if (confirm('Insert url?')) {
      e.preventDefault();
      content = `[${URL_TAG} href="${clipboardData}"][/${URL_TAG}]`;
      isDefaultHttpUrl = true;
    }
  }
  if (content) {
    setContent((prevState) =>
      replaceWithin(prevState, selectionRange[0], selectionRange[1], content)
    );
    // default to setting cursor position after the closing brackets (content length)
    let cursorPos = selectionRange[0] + content.length;

    // if we have a URL, set the cursor to between the url tags
    // so they fill out the text
    // this means subtracing the length of the url end tag
    // from the default cursor position (after the url end tag)
    if (isDefaultHttpUrl) {
      // end tag length is the tag name + "[/]".length = 3
      const endUrlTagLength = URL_TAG.length + 3;
      cursorPos -= endUrlTagLength;
    }
    modifySelectionRange(e.target, cursorPos);
  }
};

function selectionRangeValid(selectionRange) {
  return selectionRange[0] !== undefined && selectionRange[1] !== undefined;
}

function selectionRangeIsCursor(selectionRange) {
  return selectionRange[0] === selectionRange[1];
}

export const insertTag = (type, content, setContent, selectionRange, setSelectionRange, input) => {
  const bbArgs = {}; // Args for this tag
  const bbAttribs = []; // Attributes for this tag

  // Handle tags that require args here
  if (type === 'url_smart') {
    type = 'url'; // Slam down to regular url tag so the rest of the processing happens
    bbAttribs.push('smart');
  }

  if (type === 'url') {
    let selection = '';
    if (selectionRangeValid(selectionRange) && !selectionRangeIsCursor(selectionRange)) {
      selection = input.value.substring(selectionRange[0], selectionRange[1]);
    }

    // If they're selecting something and httpRegex says it's a link, skip the prompt
    if (!httpRegex.test(selection)) {
      // If they're inserting a URL, prompt them to enter the link
      // If they don't enter one, then they might already be selecting a text URL and it'll work without href
      const href = prompt('Insert link:', '');
      if (href?.length > 0) {
        bbArgs.href = `${href}`;
      }
    }
  }

  // Add all the args and attributes to the start tag
  let startTag = type;
  for (const key of Object.keys(bbArgs)) {
    startTag = startTag.concat(' ', key, '=', `"${bbArgs[key]}"`);
  }
  for (const attrib of bbAttribs) {
    startTag = startTag.concat(' ', attrib);
  }

  if (type === 'li') {
    setContent((prevState) => `${prevState}[ul][li][/li][/ul]`);
    // } else if (type === 'quote') {
    //   const { threadPage, threadId, postId, username, postContent, mentionsUser } = content;
    //   setContent((prevState) =>
    //     `${prevState} [quote mentionsUser="${mentionsUser}" postId="${postId}" threadPage="${threadPage}" threadId="${threadId}" username="${username}"]${postContent}[/quote]`.trim()
    //   );
  } else if (selectionRangeValid(selectionRange)) {
    setContent((prevState) =>
      replaceBetween(prevState, selectionRange[0], selectionRange[1], [
        `[${startTag}]`,
        `[/${type}]`,
      ])
    );
    const startTagLength = `[${startTag}]`.length;
    if (selectionRangeIsCursor(selectionRange)) {
      setSelectionRange([selectionRange[0] + startTagLength, selectionRange[0] + startTagLength]);
      modifySelectionRange(input, selectionRange[0] + startTagLength);
    } else {
      const endTagLength = `[/${type}]`.length;
      setSelectionRange([
        selectionRange[1] + startTagLength + endTagLength,
        selectionRange[1] + startTagLength + endTagLength,
      ]);
      modifySelectionRange(input, selectionRange[1] + startTagLength + endTagLength);
    }
  } else {
    const updatedContent = `${content}[${startTag}][/${type}]`;
    setContent((prevState) => `${prevState}[${startTag}][/${type}]`);
    const tagLength = `[/${type}]`.length;
    const contentLength = updatedContent.length;

    setSelectionRange([contentLength - tagLength, contentLength - tagLength]);
    modifySelectionRange(input, contentLength - tagLength);
  }
};

export function checkForHotkeys(
  e,
  content,
  setContent,
  selectionRange,
  setSelectionRange,
  input,
  handleSubmit
) {
  if (isBoldHotkey(e)) {
    e.preventDefault();
    insertTag('b', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isItalicHotkey(e)) {
    e.preventDefault();
    insertTag('i', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isUnderlinedHotkey(e)) {
    e.preventDefault();
    insertTag('u', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isCodeHotkey(e)) {
    e.preventDefault();
    insertTag('code', content, setContent, selectionRange, setSelectionRange, input);
  } else if (isStrikethroughHotkey(e)) {
    e.preventDefault();
    insertTag('s', content, setContent, selectionRange, setSelectionRange, input);
  } else if (handleSubmit !== undefined && isSubmitHotkey(e)) {
    handleSubmit();
  }
}
