import React, { useRef } from 'react';
import PropTypes from 'prop-types';

import config from '../../../../config';
import Tooltip from '../../Tooltip';
import { insertTag } from '../helpers';
import uploadImage from '../../../services/images';
import { pushSmartNotification } from '../../../utils/notification';

const ToolbarButton = ({ icon, tooltipText, iconFamily, handleButtonClick }) => (
  <Tooltip text={tooltipText}>
    <button type="button" onClick={handleButtonClick} aria-label={tooltipText}>
      <i className={`${iconFamily} fa-${icon}`} />
    </button>
  </Tooltip>
);

ToolbarButton.propTypes = {
  icon: PropTypes.string.isRequired,
  tooltipText: PropTypes.string.isRequired,
  handleButtonClick: PropTypes.func.isRequired,
  iconFamily: PropTypes.string,
};

ToolbarButton.defaultProps = {
  iconFamily: 'fas',
};

const EditorToolbar = ({ content, setContent, selectionRange, setSelectionRange, input }) => {
  const postImageUpload = useRef();

  const handleButtonClick = (type) => {
    insertTag(type, content, setContent, selectionRange, setSelectionRange, input);
  };

  const handleUploadImage = async (e) => {
    const response = await uploadImage({ imageBlob: e.target.files[0] });
    if (response?.data?.fileName) {
      pushSmartNotification({ message: 'Image uploaded successfully!' });
      const imageHttpUrl = `${config.cdnHost}/image/${response.data.fileName}`;
      setContent(`${content}[img]${imageHttpUrl}[/img]`);
    } else {
      pushSmartNotification({ error: `Failed to upload image: ${response.data.message}` });
    }
  };

  return (
    <div className="toolbar">
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('b')}
        icon="bold"
        tooltipText="Bold"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('i')}
        icon="italic"
        tooltipText="Italic"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('u')}
        icon="underline"
        tooltipText="Underlined"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('s')}
        icon="strikethrough"
        tooltipText="Strikethrough"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('url')}
        icon="link"
        tooltipText="Insert link"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('url_smart')}
        icon="globe"
        tooltipText="Insert smart link"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('code')}
        icon="code"
        tooltipText="Code"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('spoiler')}
        icon="eye-slash"
        tooltipText="Spoiler"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('h1')}
        icon="heading"
        tooltipText="Very Large Text"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('h2')}
        icon="font"
        tooltipText="Large Text"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('blockquote')}
        icon="quote-right"
        tooltipText="Quote"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('li')}
        icon="list-ul"
        tooltipText="Unordered List"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('img')}
        icon="image"
        tooltipText="Image"
      />
      <ToolbarButton
        handleButtonClick={() => postImageUpload.current.click()}
        icon="upload"
        tooltipText="Upload an image"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('youtube')}
        icon="youtube"
        tooltipText="Youtube"
        iconFamily="fab"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('twitter')}
        icon="twitter"
        tooltipText="Twitter"
        iconFamily="fab"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('video')}
        icon="video"
        tooltipText="Video (webm/mp4)"
      />
      <ToolbarButton
        handleButtonClick={() => handleButtonClick('noparse')}
        icon="terminal"
        tooltipText="Noparse (do not parse BBCode)"
      />
      <input
        ref={postImageUpload}
        className="upload"
        type="file"
        name="post-image"
        accept="image/*"
        onChange={(e) => handleUploadImage(e)}
      />
    </div>
  );
};

EditorToolbar.propTypes = {
  content: PropTypes.string.isRequired,
  setContent: PropTypes.func.isRequired,
  selectionRange: PropTypes.arrayOf(PropTypes.number).isRequired,
  setSelectionRange: PropTypes.func.isRequired,
  input: PropTypes.instanceOf(Element),
};

EditorToolbar.defaultProps = {
  input: undefined,
};

export default EditorToolbar;
