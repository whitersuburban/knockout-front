import { CalendarEvent } from 'knockout-schema';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import isBetween from 'dayjs/plugin/isBetween';
import dayjs from 'dayjs';
import { getCalendarEvents } from '../../../services/calendarEvents';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeHeadline,
  ThemeFontSizeLarge,
  ThemeFontSizeSmall,
  ThemeHighlightWeaker,
  ThemeMemberColor,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';

dayjs.extend(isBetween);

const StyledUpcomingEvents = styled.div`
  margin-bottom: 8px;

  .upcoming-events-header {
    background: ${ThemeBackgroundLighter};
    font-size: ${ThemeFontSizeLarge};
    font-weight: bold;
    padding: calc(${ThemeVerticalPadding} * 1.5) ${ThemeVerticalPadding};
  }

  .header-link {
    font-weight: normal;
    opacity: 0.3;
    font-size: ${ThemeFontSizeSmall};
    margin-left: 10px;
  }

  .upcoming-events-body {
    display: flex;
    align-items: center;
    justify-content: space-between;
    background: ${ThemeBackgroundDarker};
    padding: calc(${ThemeVerticalPadding} * 1.5) ${ThemeVerticalPadding};
    position: relative;
  }

  .upcoming-event {
    display: flex;
    align-items: center;
    overflow: hidden;
  }

  .nav-arrow {
    padding: 0 4px;
    display: flex;
    height: 100%;
    width: 27px;
  }

  .next {
    padding-right: 0;
  }

  .prev {
    padding-left: 0;
  }

  .arrow {
    background: none;
    border: none;
    outline: none;
    cursor: pointer;
    color: ${ThemeTextColor};
    font-size: 30px;
    opacity: 0.3;
    transition: 0.3s;
    padding: 0;
  }

  .arrow:hover {
    opacity: 1;
  }

  .event-start {
    font-size: ${ThemeFontSizeSmall};
    margin-bottom: 7px;
  }

  .start-info {
    display: inline-block;
    background: ${ThemeBackgroundLighter};
    padding: 6px 8px;
  }

  .live-text {
    display: inline-block;
    background: ${ThemeHighlightWeaker};
    padding: 6px;
    font-weight: bold;
    text-transform: uppercase;
  }

  .event-date {
    text-align: center;
    font-weight: bold;
    text-transform: uppercase;
    margin-right: 20px;
  }

  .event-month {
    font-size: 1.15rem;
    margin-bottom: 5px;
  }

  .event-day {
    font-size: ${ThemeFontSizeHeadline};
  }

  .event-info {
    overflow: hidden;
  }

  .event-title {
    font-weight: 600;
    font-size: ${ThemeFontSizeLarge};
    margin-bottom: 7px;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    line-height: normal;
  }

  .event-time {
    opacity: 0.4;
    margin-bottom: 7px;
  }

  .event-link {
    color: ${ThemeMemberColor};
  }

  .expand-button {
    background: none;
    border: none;
    color: ${ThemeTextColor};
    font-size: 16px;
    cursor: pointer;
    float: right;
  }

  .no-events {
    font-size: ${ThemeFontSizeLarge};
    text-align: center;
    font-weight: bold;
    width: 100%;
  }

  .no-events-text {
    opacity: 0.8;
  }

  .no-events-icon {
    font-size: 40px;
    opacity: 0.3;
    margin-bottom: 10px;
  }
`;

const UpcomingEvents = () => {
  const [events, setEvents] = useState<CalendarEvent[]>([]);
  const [currentEventIndex, setCurrentEventIndex] = useState<number>(0);

  const monthFormat = new Intl.DateTimeFormat(undefined, {
    month: 'short',
  });

  const dateFormat = new Intl.DateTimeFormat(undefined, {
    day: 'numeric',
  });

  const dateTimeFormat = new Intl.DateTimeFormat(undefined, {
    day: 'numeric',
    month: 'short',
    hour: 'numeric',
    minute: 'numeric',
  });

  const timeFormat = new Intl.DateTimeFormat(undefined, {
    hour: 'numeric',
    minute: 'numeric',
  });

  const fetchCalendarEvents = async () => {
    const startDate = new Date();
    startDate.setDate(startDate.getDate() - 3);

    const endDate = new Date();
    endDate.setDate(endDate.getDate() + 14);
    const results = await getCalendarEvents(startDate, endDate);
    setEvents(results.filter((result) => dayjs(result.endsAt).isAfter(dayjs())).slice(0, 4));
  };

  useEffect(() => {
    fetchCalendarEvents();
  }, []);

  const event = events[currentEventIndex];
  const startDate = new Date(event?.startsAt);
  const endDate = new Date(event?.endsAt);

  let isLive = false;
  let prefix;
  if (dayjs(startDate).isAfter(dayjs())) {
    prefix = 'Starts';
  } else if (dayjs(endDate).isAfter(dayjs())) {
    prefix = 'Ends';
    isLive = true;
  } else {
    prefix = 'Ended';
  }

  return (
    <StyledUpcomingEvents>
      <div className="upcoming-events-header">
        <span>Upcoming Events</span>
        <Link className="header-link" to="/calendar">
          Show all
        </Link>
      </div>

      <div className="upcoming-events-body">
        {event && (
          <>
            <div className="nav-arrow prev">
              {events.length > 1 && currentEventIndex > 0 && (
                <button
                  className="arrow"
                  type="button"
                  onClick={() => setCurrentEventIndex((currentEventIndex - 1) % events.length)}
                >
                  <i className="fa-solid fa-chevron-left" />
                </button>
              )}
            </div>
            <div className="upcoming-event" key={event.id}>
              <div className="event-date">
                <div className="event-month">{monthFormat.format(startDate)}</div>
                <div className="event-day">{dateFormat.format(startDate)}</div>
              </div>
              <div className="event-info">
                <div className="event-start">
                  {dayjs().isBetween(startDate, endDate, null, '[)') && (
                    <span className="live-text">Live</span>
                  )}
                  <span className="start-info">
                    {`${prefix} ${dayjs(isLive ? endDate : startDate).fromNow()}`}
                  </span>
                </div>
                <div className="event-title" title={event.title}>
                  {event.title}
                </div>
                <div className="event-time">
                  {`${timeFormat.format(startDate)} - ${(startDate.getDate() === endDate.getDate()
                    ? timeFormat
                    : dateTimeFormat
                  ).format(endDate)}`}
                </div>
                <div className="event-link">
                  <Link to={`/thread/${event.thread.id}`}>View Thread &gt;&gt;</Link>
                </div>
              </div>
            </div>
            <div className="nav-arrow next">
              {events.length > 1 && currentEventIndex < events.length - 1 && (
                <button
                  className="arrow"
                  type="button"
                  onClick={() => setCurrentEventIndex((currentEventIndex + 1) % events.length)}
                >
                  <i className="fa-solid fa-chevron-right" />
                </button>
              )}
            </div>
          </>
        )}
        {events.length === 0 && (
          <div className="no-events">
            <i className="fa-solid fa-calendar-days no-events-icon" />
            <div className="no-events-text">No upcoming events</div>
          </div>
        )}
      </div>
    </StyledUpcomingEvents>
  );
};

export default UpcomingEvents;
