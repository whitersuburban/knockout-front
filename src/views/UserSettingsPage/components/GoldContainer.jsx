import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Panel, PanelTitle } from '../../../components/Panel';
import {
  ThemeHighlightStronger,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import { cancelKnockoutGoldSubscription } from '../../../services/knockoutGold';
import { syncData } from '../../../services/user';
import { aprilFools } from '../../../utils/eventDates';

const KnockoutGoldButton = styled.button`
  display: block;
  position: relative;
  background: ${(props) => (props.deleting ? '#ad1a1a' : ThemeHighlightStronger(props))};
  color: white;
  border: none;
  border-radius: 5px;
  margin: 9px auto;
  padding: 0 0 0 32px;
  font-size: 13px;
  text-align: center;
  width: 230px;
  height: 32px;
  line-height: 32px;
  cursor: pointer;
  overflow: hidden;

  &:active {
    opacity: 0.9;
  }

  i {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 32px;
    line-height: 32px;
    text-align: center;
    background: rgba(0, 0, 0, 0.1);
  }

  span {
    padding: 0 10px;
  }
`;

const StyledGoldDescription = styled.div`
  display: flex;
  padding: ${ThemeHorizontalPadding} ${ThemeVerticalPadding};
  padding-top: 0;
  flex-direction: row;
  flex-wrap: wrap;

  ul {
    display: block;
    width: 100%;
  }

  .col {
    flex: 1 1 30%;
    margin-bottom: ${ThemeVerticalPadding};

    .description-header {
      font-weight: bold;
    }
  }

  .thanks-msg {
    width: 100%;
    margin-bottom: ${ThemeVerticalPadding};
  }

  .cancel-gold-btn {
    margin-top: ${ThemeVerticalPadding};
  }
`;

const formatDateFromIso = (isoDateString) => isoDateString.split('T').shift();

const GoldContainer = ({ handleSubscriptionClick, subscription }) => {
  if (subscription?.status !== 'active') {
    return (
      <Panel>
        <PanelTitle title="Knockout Gold">Knockout Gold</PanelTitle>
        <StyledGoldDescription>
          This is a monthly £3.50 donation to the website that gives you special perks on Knockout!
          If you are a new or regular user, you&apos;ll gain access to:
          <ul>
            <li>A glowing gold name</li>
            {aprilFools() && <li>A blue checkmark</li>}
            <li>Thread background customization</li>
            {aprilFools() && <li>Coming soon: The exclusive ability to rate posts!</li>}
            <li>Animated GIF avatars</li>
          </ul>
          Donations are used to keep Knockout up and running, and to donate to community causes when
          we can.
          <br />
        </StyledGoldDescription>

        <KnockoutGoldButton background="#ad1a1a" onClick={handleSubscriptionClick} type="button">
          <i className="fa-solid fa-star" />
          Donate with Knockout Gold
        </KnockoutGoldButton>
      </Panel>
    );
  }

  const handleCancel = async () => {
    // eslint-disable-next-line no-alert
    if (window.confirm('Are you sure you want to cancel your Knockout Gold subscription?')) {
      await cancelKnockoutGoldSubscription();
      await syncData();
      window.location.reload();
    }
  };

  const { status, startedAt, canceledAt, nextPaymentAt } = subscription;

  const formattedStatus = status === 'active' ? 'Active' : 'Inactive';

  return (
    <Panel>
      <PanelTitle title="Knockout Gold">Your Knockout Gold Subscription</PanelTitle>
      <StyledGoldDescription>
        <div className="col">
          <div className="description-header">Status</div>
          <div className="description-value">{formattedStatus}</div>
        </div>
        <div className="col">
          <div className="description-header">Start date</div>
          <div className="description-value">{formatDateFromIso(startedAt)}</div>
        </div>
        <div className="col">
          <div className="description-header">Next payment date</div>
          <div className="description-value">{formatDateFromIso(nextPaymentAt)}</div>
        </div>
        {canceledAt && (
          <div className="col">
            <div className="description-header">Next payment date</div>
            <div className="description-value">{formatDateFromIso(nextPaymentAt)}</div>
          </div>
        )}
        <div className="thanks-msg">
          Thank you for donating to Knockout! To cancel your subscription, click Cancel Subscription
          below.
        </div>
        <KnockoutGoldButton
          className="cancel-gold-btn"
          deleting
          background="#ad1a1a"
          onClick={handleCancel}
          type="button"
        >
          <i className="fa-solid fa-times" />
          Cancel Subscription
        </KnockoutGoldButton>
      </StyledGoldDescription>
    </Panel>
  );
};

GoldContainer.propTypes = {
  handleSubscriptionClick: PropTypes.func,
  subscription: PropTypes.shape({
    status: PropTypes.string,
    startedAt: PropTypes.string,
    canceledAt: PropTypes.string,
    nextPaymentAt: PropTypes.string,
  }),
};

GoldContainer.defaultProps = {
  handleSubscriptionClick: null,
  subscription: null,
};

export default GoldContainer;
