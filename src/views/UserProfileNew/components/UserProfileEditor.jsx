import React, { useState, useEffect, useRef, useContext } from 'react';
import PropTypes from 'prop-types';

import { useSelector } from 'react-redux';
import styled, { ThemeContext } from 'styled-components';
import { rgba } from 'polished';
import {
  removeUserHeader,
  updateUser,
  updateUserBackground,
  updateUserHeader,
  updateUserProfile,
} from '../../../services/user';

import Modal from '../../../components/Modals/Modal';
import ModalRadioButton from '../../../components/Modals/ModalRadioButton';
import { FieldLabelSmall, TextField, TextFieldLarge } from '../../../components/FormControls';
import { pushSmartNotification } from '../../../utils/notification';
import { checkBetweenLengths, checkValidURL, validate } from '../../../utils/forms';
import InputError, { StyledInputError } from '../../../components/InputError';
import config from '../../../../config';
import {
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
} from '../../../utils/ThemeNew';
import { MobileMediaQuery } from '../../../components/SharedStyles';
import { TextButton } from '../../../components/Buttons';
import uploadAvatar, { setEmptyAvatar } from '../../../services/avatar';
import Tooltip from '../../../components/Tooltip';
import FormSwitch from '../../../components/FormControls/components/FormSwitch';
import { roleCheck } from '../../../components/UserRoleRestricted';
import { GOLD_USER_ROLES } from '../../../utils/roleCodes';
import { BIO_TITLE_LENGTH } from '../../../utils/user';

const LinkRow = styled.div`
  margin-bottom: 20px;

  .link-input {
    display: flex;
    align-items: center;
    margin-bottom: 10px;
  }

  .link-icon {
    opacity: 0.6;
    margin-right: 12px;
    font-size: 20px;
    width: 20px;
    text-align: center;
  }

  ${StyledInputError}, .pronoun-tip {
    margin-left: 32px;
  }

  .pronoun-field {
    margin-bottom: 0;
  }

  .pronoun-tip {
    font-size: calc(${ThemeFontSizeSmall} * 0.9);
    opacity: 0.5;
  }
`;

const SwitchControl = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 20px;

  .label {
    font-size: ${ThemeFontSizeMedium};
    margin-left: 10px;
  }
`;

const ProfilePreview = styled.div`
  .header {
    margin: -15px;
    margin-bottom: 0px;
    overflow: hidden;
    max-height: 141px;
    display: flex;
    align-items: center;
    position: relative;
  }

  .header-image {
    width: 100%;
    display: block;
  }

  .header-tooltip {
    .tooltip-text {
      top: 0;
    }
  }

  .avatar {
    background: ${ThemeBackgroundLighter};
    width: 76px;
    height: 76px;
    margin-top: -36px;
    margin-bottom: 25px;
    position: relative;
    z-index: 1;
  }

  .upload-button-container {
    position: absolute;
    width: 100%;
    height: 100%;
    background: ${(props) => rgba(props.theme.mode === 'light' ? '#818181' : 'black', 0.5)};
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 1;
  }

  .upload-button {
    color: white;
    font-size: 18px;
    opacity: 0.5;
  }

  .remove-button {
    color: white;
    font-size: 14px;
    position: absolute;
    top: 0;
    right: 0;
    padding: 10px;
    opacity: 0.5;
  }

  .upload {
    display: none;
  }

  .avatar-image {
    width: 100%;
    height: 100%;
    object-fit: contain;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
  }

  .profile-content {
    position: relative;
    z-index: 2;
    display: grid;
    grid-template-columns: 250px 1fr;
    column-gap: 75px;
    padding: 0 20px;
    padding-bottom: 20px;
    ${(props) =>
      props.theme.mode === 'light' &&
      'background: rgba(0, 0, 0, 0) linear-gradient(rgb(255, 255, 255) 0%, rgba(255, 255, 255, 0.6) 27%) repeat scroll 0% 0%;'}
    margin-top: -3px;
    ${MobileMediaQuery} {
      grid-template-columns: 1fr;
    }
  }
`;

const UserProfileEditor = ({
  closeFn,
  isOpen,
  profile,
  headerImage,
  callback,
  userPronouns,
  userUseBioForTitle,
  userShowOnlineStatus,
}) => {
  const { id: userId, ...user } = useSelector((state) => state.user);
  const url = `${config.cdnHost}/image/${user.avatarUrl}`;
  const hasAvatar =
    user.avatarUrl && user.avatarUrl.length !== 0 && !user.avatarUrl.includes('none.webp');

  const theme = useContext(ThemeContext);
  const defaultHeader =
    theme.mode === 'light' ? 'static/profile_header.png' : 'static/profile_header_dark.png';

  const [enableComments, setEnableComments] = useState(true);
  const [bio, setBio] = useState('');
  const [useBioForTitle, setUseBioForTitle] = useState(userUseBioForTitle);
  const [avatar, setAvatar] = useState();
  const [pronouns, setPronouns] = useState('');
  const [showOnlineStatus, setShowOnlineStatus] = useState(false);
  const [avatarUrl, setAvatarUrl] = useState(url);
  const [removeAvatar, setRemoveAvatar] = useState(false);
  const [header, setHeader] = useState();
  const [headerUrl, setHeaderUrl] = useState(headerImage);
  const [removeHeader, setRemoveHeader] = useState(false);
  const [website, setWebsite] = useState('');
  const [steam, setSteam] = useState('');
  const [discord, setDiscord] = useState('');
  const [github, setGithub] = useState('');
  const [twitter, setTwitter] = useState('');
  const [twitch, setTwitch] = useState('');
  const [gitlab, setGitlab] = useState('');
  const [tumblr, setTumblr] = useState('');
  const [backgroundType, setBackgroundType] = useState('cover');
  const [backgroundImage, setBackgroundImage] = useState(null);
  const [errors, setErrors] = useState({});

  const avatarUpload = useRef();
  const headerUpload = useRef();

  const profileValidators = {
    website: [checkValidURL],
    steam: [(value) => checkValidURL(value, 'steamcommunity.com')],
    discord: [(value) => checkBetweenLengths(value, 0, 37)],
    github: [(value) => checkBetweenLengths(value, 0, 35)],
    twitter: [(value) => checkBetweenLengths(value, 0, 15)],
    twitch: [(value) => checkBetweenLengths(value, 0, 25)],
    gitlab: [(value) => checkBetweenLengths(value, 0, 35)],
    tumblr: [(value) => checkBetweenLengths(value, 0, 32)],
    pronouns: [(value) => checkBetweenLengths(value, 0, 25)],
  };

  useEffect(() => {
    const results = validate(
      {
        website,
        steam,
        discord,
        github,
        twitter,
        twitch,
        gitlab,
        tumblr,
        pronouns,
      },
      profileValidators
    );
    setErrors(results);
  }, [website, steam, discord, github, twitter, twitch, gitlab, tumblr, pronouns]);

  useEffect(() => {
    if (isOpen) {
      setAvatar();
      setAvatarUrl(url);
      setHeader();
      setHeaderUrl(headerImage);
      setBio(profile.bio || '');
      setWebsite(profile.social?.website || '');
      setSteam(profile.social?.steam?.url);
      setDiscord(profile.social?.discord || '');
      setGithub(profile.social?.github || '');
      setTwitter(profile.social?.twitter || '');
      setTwitch(profile.social?.twitch || '');
      setGitlab(profile.social?.gitlab || '');
      setTumblr(profile.social?.tumblr || '');
      setBackgroundType(profile.background?.type || 'cover');
      setEnableComments(!profile.disableComments);
      setPronouns(userPronouns);
      setShowOnlineStatus(userShowOnlineStatus);
      setUseBioForTitle(!!userUseBioForTitle);
    }
  }, [profile, isOpen, userUseBioForTitle]);

  const setImage = (image, setUrl) => {
    if (image) {
      const reader = new FileReader();

      reader.onload = (e) => {
        setUrl(e.target.result);
      };

      reader.readAsDataURL(image);
    }
  };

  const toggleComments = () => {
    setEnableComments((value) => !value);
  };

  const toggleUseBioForTitle = () => {
    setUseBioForTitle((value) => !value);
  };

  const toggleShowOnlineStatus = () => {
    setShowOnlineStatus((value) => !value);
  };

  useEffect(() => {
    setImage(avatar, setAvatarUrl);
    setRemoveAvatar(false);
  }, [avatar]);

  useEffect(() => {
    setImage(header, setHeaderUrl);
    setRemoveHeader(false);
  }, [header]);

  useEffect(() => {
    setHeaderUrl(headerImage);
  }, [headerImage]);

  useEffect(() => {
    if (removeAvatar) setAvatarUrl(`${config.cdnHost}/image/none.webp`);
  }, [removeAvatar]);

  useEffect(() => {
    if (removeHeader) setHeaderUrl(defaultHeader);
  }, [removeHeader, defaultHeader]);

  const handleSubmit = async () => {
    try {
      await updateUserProfile(userId, {
        bio,
        social: {
          website,
          steam: {
            url: steam,
          },
          discord,
          github,
          twitter,
          twitch,
          gitlab,
          tumblr,
        },
        disableComments: !enableComments,
        useBioForTitle,
      });

      if (roleCheck(GOLD_USER_ROLES)) {
        await updateUserBackground(userId, {
          image: backgroundImage,
          type: backgroundType,
        });
      }

      let newAvatar = user.avatarUrl;
      if (avatar) {
        const result = await uploadAvatar(avatar);
        newAvatar = `${result.message}?t=${new Date().getTime()}`;
        await updateUser({ avatarUrl: result.message }, userId);
      } else if (hasAvatar && removeAvatar) {
        newAvatar = 'none.webp';
        await setEmptyAvatar(userId);
      }

      if (pronouns !== userPronouns) {
        await updateUser({ pronouns }, userId);
      }

      if (showOnlineStatus !== userShowOnlineStatus) {
        await updateUser({ showOnlineStatus }, userId);
      }

      let newHeader = profile.header;
      if (header) {
        const result = await updateUserHeader(userId, header);
        newHeader = `${result.header}?t=${new Date().getTime()}`;
      } else if (profile.header !== undefined && removeHeader) {
        newHeader = undefined;
        await removeUserHeader(userId);
      }

      let steamUrl = steam;
      if (steamUrl?.slice(-1) === '/') {
        steamUrl = steamUrl.slice(0, -1);
      }
      callback(
        {
          bio,
          social: {
            website,
            discord,
            steam: { name: steamUrl?.split('/').slice(-1)[0], url: steam },
            github,
            twitter,
            twitch,
            gitlab,
            tumblr,
          },
          background: {
            type: backgroundType,
            url: profile.background?.url,
          },
          header: newHeader,
          disableComments: !enableComments,
        },
        newAvatar,
        pronouns,
        useBioForTitle,
        showOnlineStatus
      );
      closeFn();
    } catch (error) {
      console.log(error);
      pushSmartNotification({ error: 'Could not update profile.' });
    }
  };

  return (
    <Modal
      title="Edit your profile"
      cancelFn={closeFn}
      submitFn={handleSubmit}
      disableSubmit={Object.keys(errors).length > 0}
      width={650}
      isOpen={isOpen}
    >
      <ProfilePreview>
        <div className="header">
          <div className="upload-button-container">
            <Tooltip className="header-tooltip" text="Recommended size: 1440x320">
              <TextButton
                title="Upload header"
                className="upload-button"
                onClick={() => headerUpload.current.click()}
              >
                <i className="fa-solid fa-camera" />
              </TextButton>
            </Tooltip>
            {profile.header !== undefined && !removeHeader && (
              <TextButton
                title="Remove header"
                className="remove-button"
                onClick={() => setRemoveHeader(true)}
              >
                <i className="fa-solid fa-times" />
              </TextButton>
            )}
          </div>
          <img className="header-image" alt="Profile header" src={headerUrl} />
          <input
            ref={headerUpload}
            className="upload"
            type="file"
            name="header"
            accept="image/*"
            onChange={(e) => setHeader(e.target.files[0])}
          />
        </div>
        <div className="avatar">
          <div className="upload-button-container">
            <Tooltip text="Recommended size: 115x115" top right>
              <TextButton
                title="Upload avatar"
                className="upload-button"
                onClick={() => avatarUpload.current.click()}
              >
                <i className="fa-solid fa-camera" />
              </TextButton>
            </Tooltip>
            {hasAvatar && !removeAvatar && (
              <TextButton
                title="Remove avatar"
                className="remove-button"
                style={{ padding: '5px 7px' }}
                onClick={() => setRemoveAvatar(true)}
              >
                <i className="fa-solid fa-times" />
              </TextButton>
            )}
          </div>
          {(hasAvatar || avatar) && (
            <img className="avatar-image" src={avatarUrl} alt={`${user.username}'s Avatar`} />
          )}
          <input
            ref={avatarUpload}
            className="upload"
            type="file"
            name="avatar"
            accept="image/*"
            onChange={(e) => setAvatar(e.target.files[0])}
          />
        </div>
      </ProfilePreview>
      <SwitchControl>
        <FormSwitch checked={enableComments} toggle={toggleComments} />
        <span className="label">Enable comments</span>
      </SwitchControl>
      <SwitchControl>
        <FormSwitch checked={showOnlineStatus} toggle={toggleShowOnlineStatus} />
        <span className="label">Show Online Indicator</span>
      </SwitchControl>
      <TextFieldLarge
        value={bio}
        maxLength={160}
        placeholder="Add your bio"
        onChange={(e) => setBio(e.target.value)}
      />
      <SwitchControl>
        <FormSwitch checked={useBioForTitle} toggle={toggleUseBioForTitle} />
        <span className="label">{`Display first line or ${BIO_TITLE_LENGTH} characters of bio as your title`}</span>
      </SwitchControl>
      <LinkRow>
        <div className="link-input pronoun-field">
          <i className="fa-solid fa-heart link-icon" />
          <TextField
            value={pronouns}
            maxLength={25}
            placeholder="Pronouns"
            onChange={(e) => setPronouns(e.target.value)}
          />
        </div>
        <span className="pronoun-tip">What others should refer to you as; do not misuse</span>
        <InputError error={errors.pronouns} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fa-solid fa-link link-icon" />
          <TextField
            value={website}
            placeholder="Website"
            onChange={(e) => setWebsite(e.target.value)}
          />
        </div>
        <InputError error={errors.website} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fa-brands fa-steam link-icon" />
          <TextField
            value={steam}
            placeholder="Steam profile URL"
            onChange={(e) => setSteam(e.target.value)}
          />
        </div>
        <InputError error={errors.steam} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fa-brands fa-discord link-icon" />
          <TextField
            value={discord}
            placeholder="Discord username"
            onChange={(e) => setDiscord(e.target.value)}
          />
        </div>
        <InputError error={errors.discord} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fa-brands fa-github link-icon" />
          <TextField
            value={github}
            placeholder="Github username"
            onChange={(e) => setGithub(e.target.value)}
          />
        </div>
        <InputError error={errors.github} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fa-brands fa-twitter link-icon" />
          <TextField
            value={twitter}
            placeholder="Twitter username"
            onChange={(e) => setTwitter(e.target.value)}
          />
        </div>
        <InputError error={errors.twitter} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fa-brands fa-twitch link-icon" />
          <TextField
            value={twitch}
            placeholder="Twitch username"
            onChange={(e) => setTwitch(e.target.value)}
          />
        </div>
        <InputError error={errors.twitch} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fa-brands fa-gitlab link-icon" />
          <TextField
            value={gitlab}
            placeholder="Gitlab username"
            onChange={(e) => setGitlab(e.target.value)}
          />
        </div>
        <InputError error={errors.gitlab} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fa-brands fa-tumblr-square link-icon" />
          <TextField
            value={tumblr}
            placeholder="Tumblr username"
            onChange={(e) => setTumblr(e.target.value)}
          />
        </div>
        <InputError error={errors.tumblr} />
      </LinkRow>
      {roleCheck(GOLD_USER_ROLES) && (
        <>
          <FieldLabelSmall>Background type</FieldLabelSmall>
          <ModalRadioButton
            name="backgroundType"
            property={backgroundType}
            values={['cover', 'tiled']}
            onChange={(e) => setBackgroundType(e.target.value)}
          />
          <FieldLabelSmall>Background image (max size ~2MB)</FieldLabelSmall>
          <input
            name="backgroundImage"
            type="file"
            onChange={(e) => setBackgroundImage(e.target.files[0])}
          />
        </>
      )}
    </Modal>
  );
};

UserProfileEditor.propTypes = {
  closeFn: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  profile: PropTypes.shape({
    bio: PropTypes.string,
    // eslint-disable-next-line react/forbid-prop-types
    social: PropTypes.object,
    background: PropTypes.shape({
      type: PropTypes.string,
      url: PropTypes.string,
    }),
    header: PropTypes.string,
    disableComments: PropTypes.bool,
  }).isRequired,
  headerImage: PropTypes.string.isRequired,
  callback: PropTypes.func.isRequired,
  userPronouns: PropTypes.string,
  userUseBioForTitle: PropTypes.bool,
  userShowOnlineStatus: PropTypes.bool,
};

UserProfileEditor.defaultProps = {
  userPronouns: '',
  userUseBioForTitle: false,
  userShowOnlineStatus: false,
};

export default UserProfileEditor;
