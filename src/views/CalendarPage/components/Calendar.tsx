import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { CalendarEvent } from 'knockout-schema';
import {
  ThemeBackgroundLighter,
  ThemeFontSizeHuge,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import MiniCalendarDay from './MiniCalendarDay';
import { formatShortDate } from '../../../utils/dateFormat';

dayjs.extend(relativeTime);

const INITIAL_NAVIGATION_DISABLE_MS = 750;

const StyledCalendar = styled.div`
  background-color: ${ThemeBackgroundLighter};
  padding: ${ThemeHorizontalPadding};
  margin: ${ThemeHorizontalPadding};

  h2 {
    font-size: ${ThemeFontSizeHuge};
  }

  .calendar-header {
    margin-bottom: ${ThemeVerticalPadding};
    display: flex;
    text-align: center;
    align-items: center;
    justify-content: center;

    h2 {
      margin: ${ThemeVerticalPadding} calc(${ThemeHorizontalPadding} / 2);
    }

    .month-name-small {
      font-size: ${ThemeFontSizeMedium};
      margin-left: calc(${ThemeHorizontalPadding} * 2);
      margin-right: calc(${ThemeHorizontalPadding} * 2);
      margin-top: calc(${ThemeVerticalPadding} / 2);
    }

    .nav-disabled {
      pointer-events: none;
      opacity: 0.5;
    }
  }

  .calendar-body {
    display: flex;
    flex-direction: row;
    column-count: 7;
    flex-wrap: wrap;
    justify-content: space-evenly;
    margin: ${ThemeHorizontalPadding};
    max-width: 100%;

    .calendar-day {
      flex: 1 1 12.5%;
      border: 1px solid ${ThemeTextColor};
      padding: 2px;
      font-size: ${ThemeFontSizeSmall};
    }
  }
`;

interface CalendarProps {
  date: Date;
  calendarEvents: CalendarEvent[];
  onCalendarEventClick: (event: CalendarEvent) => void;
  onDayClick: (startOfDay: Date, dayEvents: CalendarEvent[]) => void;
  canCreate: boolean;
  onCreateClick: (defaultDate: Date) => void;
}

const Calendar = ({
  date,
  calendarEvents,
  onCalendarEventClick,
  onDayClick,
  canCreate,
  onCreateClick,
}: CalendarProps) => {
  // Make sure user can't spam month navigation buttons
  const [navigationDisabled, setNavigationDisabled] = useState(true);

  useEffect(() => {
    setNavigationDisabled(true);
    const timeout = setTimeout(() => setNavigationDisabled(false), INITIAL_NAVIGATION_DISABLE_MS);
    return () => clearTimeout(timeout);
  }, [calendarEvents]);

  const currentYear = date.getFullYear();
  const currentMonth = date.getMonth() + 1;

  const previousMonth = new Date(currentYear, currentMonth - 2);
  const nextMonth = new Date(currentYear, currentMonth);

  const previousMonthString = formatShortDate(previousMonth);
  const nextMonthString = formatShortDate(nextMonth);

  const dayBuffer = new Date(currentYear, currentMonth - 1, 1).getDay();

  return (
    <StyledCalendar>
      <div className="calendar-header">
        <Link
          to={`/calendar/${previousMonthString}`}
          className={`month-name-small ${navigationDisabled ? 'nav-disabled' : ''}`}
        >
          <i className="fa-solid fa-angle-left" />
          {` ${previousMonth.toLocaleDateString('en-us', { month: 'long' })}`}
        </Link>
        <h2>{date.toLocaleDateString('en-us', { month: 'long', year: 'numeric' })}</h2>
        <Link
          to={`/calendar/${nextMonthString}`}
          className={`month-name-small ${navigationDisabled ? 'nav-disabled' : ''}`}
        >
          {`${nextMonth.toLocaleDateString('en-us', { month: 'long' })} `}
          <i className="fa-solid fa-angle-right" />
        </Link>
      </div>
      <div className="calendar-body">
        {Array.from({ length: 35 }, (_, i) => {
          const startOfDay = new Date(currentYear, currentMonth - 1, i + 1);
          startOfDay.setDate(startOfDay.getDate() - dayBuffer);

          const endOfDay = new Date(startOfDay);
          endOfDay.setHours(23, 59, 59, 999);

          const differentMonth = startOfDay.getMonth() !== date.getMonth();

          // get all calendar events that occur on this day
          const dayEvents = calendarEvents
            .filter((event) => {
              // if the event's startDate is less than the endOfDay
              // and the events endDate is greater than the endOfDay, the event occurs within the day
              const eventStart = new Date(event.startsAt);
              const eventEnd = new Date(event.endsAt);
              const sameDay = eventStart.getDate() === eventEnd.getDate();

              if (eventEnd < startOfDay || eventStart > endOfDay) {
                return false;
              }

              return !sameDay || eventEnd <= endOfDay;
            })
            .sort((eventA, eventB) => {
              if (eventA.startsAt < eventB.startsAt) {
                return -1;
              }
              if (eventA.startsAt > eventB.startsAt) {
                return 1;
              }
              return 0;
            });

          // when user creates an event for this day, default to 12pm starting time
          const defaultCreateDate = new Date(startOfDay);
          defaultCreateDate.setHours(defaultCreateDate.getHours() + 12);

          return (
            <MiniCalendarDay
              key={startOfDay.toISOString()}
              date={endOfDay}
              dayEvents={dayEvents}
              onEventClick={onCalendarEventClick}
              onDayClick={() => onDayClick(startOfDay, dayEvents)}
              differentMonth={differentMonth}
              canCreate={canCreate}
              onCreateClick={() => onCreateClick(defaultCreateDate)}
            />
          );
        })}
      </div>
    </StyledCalendar>
  );
};

export default Calendar;
