import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { createEvent, EventAttributes } from 'ics';
import { CalendarEvent } from 'knockout-schema';
import Modal from '../../../components/Modals/Modal';
import {
  ThemeBackgroundLighter,
  ThemeBannedUserColor,
  ThemeFontSizeHuge,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import UserRoleWrapper from '../../../components/UserRoleWrapper';
import ForumIcon from '../../../components/ForumIcon';

const StyledModalContent = styled.div`
  line-height: 1.6;

  hr {
    height: 2px;
    border: none;
    background-color: ${ThemeBackgroundLighter};
  }

  .description {
    margin-bottom: calc(${ThemeVerticalPadding} * 2);
    white-space: pre-wrap;
  }

  .thread-label {
    font-weight: bold;
  }

  .thread {
    .thread-icon {
      width: ${ThemeFontSizeHuge};
      vertical-align: text-bottom;
      padding-top: 5px;
      margin-right: calc(${ThemeHorizontalPadding} / 2);
    }
  }

  .event-actions {
    margin-top: ${ThemeVerticalPadding};
    width: 90%;
    font-size: ${ThemeFontSizeSmall};
    display: flex;
    justify-content: space-between;
  }

  .export-btn {
    cursor: pointer;
    text-decoration: underline;
  }

  .destroy-btn {
    margin-bottom: 0;
    cursor: pointer;
    text-decoration: underline;
    color: ${ThemeBannedUserColor};
  }

  .range {
    margin-top: ${ThemeVerticalPadding};
    margin-bottom: ${ThemeVerticalPadding};

    display: flex;

    span {
      font-weight: bold;
    }

    .from,
    .to {
      flex: 1 1 60%;
    }
  }
`;

interface CalendarEventModalProps {
  isOpen: boolean;
  close: () => void;
  event: CalendarEvent;
  canEdit: boolean;
  onEdit: () => void;
  canDestroy: boolean;
  onDestroy: () => void;
}

const CalendarEventModal = ({
  isOpen,
  close,
  event,
  canEdit,
  onEdit,
  canDestroy,
  onDestroy,
}: CalendarEventModalProps) => {
  const { createdBy, title, description, thread, startsAt, endsAt } = event;

  const timeFormatOptions: Intl.DateTimeFormatOptions = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    timeZoneName: 'short',
  };

  const startsAtDate = new Date(startsAt);
  const endsAtDate = new Date(endsAt);

  const formattedStartsAt = startsAtDate.toLocaleString('en-US', timeFormatOptions);
  const formattedEndsAt = endsAtDate.toLocaleString('en-US', timeFormatOptions);

  const diffMs = endsAtDate.getTime() - startsAtDate.getTime();
  const duration = diffMs / 60 / 1000; // event duration in minutes

  const threadUrl = `${window.location.origin}/thread/${thread.id}`;

  const downloadIcs = () => {
    const icsEvent: EventAttributes = {
      title,
      description,
      start: [
        startsAtDate.getFullYear(),
        startsAtDate.getMonth() + 1,
        startsAtDate.getDate(),
        startsAtDate.getHours(),
        startsAtDate.getMinutes(),
      ],
      location: threadUrl,
      duration: { minutes: duration },
      url: threadUrl,
    };
    createEvent(icsEvent, (error, value) => {
      if (error) {
        console.error(error);
      } else {
        window.open(`data:text/calendar;charset=utf8,${encodeURIComponent(value)}`);
      }
    });
  };

  // user role wrapper expects user to have isBanned attribute
  const createdByuser = { ...createdBy, isBanned: createdBy.banned };

  return (
    <Modal
      iconUrl="/static/icons/planner.png"
      title={title}
      isOpen={isOpen}
      cancelFn={close}
      submitFn={onEdit}
      hideButtons={!canEdit}
      submitText={canEdit ? 'Edit' : ''}
    >
      <StyledModalContent>
        <div className="description">{description}</div>
        <hr />
        <div className="thread">
          <div className="from">
            <span className="thread-label">Thread</span>
            <br />
            <Link to={`/thread/${thread.id}`} target="_blank">
              <ForumIcon iconId={thread.iconId} className="thread-icon" />
              <span className="thread-title">{thread.title}</span>
            </Link>
          </div>
        </div>

        <div className="range">
          <div className="from">
            <span>From</span>
            <br />
            {formattedStartsAt}
          </div>
          <div className="to">
            <span>To</span>
            <br />
            {formattedEndsAt}
          </div>
        </div>

        <hr />

        <div className="event-actions">
          <div className="created-by">
            Created by
            <Link to={`/user/${createdBy.id}`}>
              <UserRoleWrapper user={createdByuser}>{` ${createdBy.username}`}</UserRoleWrapper>
            </Link>
          </div>
          <div
            tabIndex={0}
            role="button"
            className="export-btn"
            onKeyUp={(e) => {
              if (e.key === 'Enter') downloadIcs();
            }}
            onClick={downloadIcs}
          >
            Download .ics file
          </div>
          {canDestroy && (
            <div
              tabIndex={0}
              role="button"
              className="destroy-btn"
              onKeyUp={(e) => {
                if (e.key === 'Enter') onDestroy();
              }}
              onClick={onDestroy}
            >
              Remove this event
            </div>
          )}
        </div>
      </StyledModalContent>
    </Modal>
  );
};

export default CalendarEventModal;
