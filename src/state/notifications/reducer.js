import {
  NOTIFICATION_READ,
  NOTIFICATION_READ_ALL,
  NOTIFICATION_SET,
  NOTIFICATION_ADD,
} from './actions';

const initialState = {
  notifications: {},
};

export default function (state = initialState, action = undefined) {
  switch (action.type) {
    case NOTIFICATION_ADD:
      return {
        ...state,
        notifications: {
          ...state.notifications,
          [`${action.value.type}:${action.value.data?.id}`]: action.value,
        },
      };
    case NOTIFICATION_SET: {
      const map = {};
      action.value.forEach((notification) => {
        map[`${notification.type}:${notification.data?.id}`] = notification;
      });
      return { ...state, notifications: map };
    }
    case NOTIFICATION_READ:
      return {
        ...state,
        notifications: {
          ...state.notifications,
          [action.value]: { ...state.notifications[action.value], read: true },
        },
      };
    case NOTIFICATION_READ_ALL: {
      const notificationList = Object.values(state.notifications);
      const newNotifications = {};
      notificationList.forEach((notification) => {
        newNotifications[`${notification.type}:${notification.data?.id}`] = {
          ...notification,
          read: true,
        };
      });
      return { ...state, notifications: newNotifications };
    }
    default:
      break;
  }
  return state;
}
