import { authPost } from '../../services/common';
import { removeUserFromStorage, saveUserToStorage, setupUserFlags } from '../../services/user';

export const USER_UPDATE = 'USER_UPDATE';
export const USER_LOGOUT = 'USER_LOGOUT';

export function userUpdate(user) {
  saveUserToStorage({ user });

  return {
    type: USER_UPDATE,
    payload: user,
  };
}

export function userLogin(user, history, dispatch) {
  dispatch(userUpdate(setupUserFlags(user)));
  if (!user.username) {
    history.push('/usersetup');
  } else {
    history.push('/');
  }
}

// User Logout action
export function userLogoutSuccess() {
  return {
    type: USER_LOGOUT,
  };
}

export function userLogout(history) {
  return (dispatch) => {
    return authPost({ url: '/auth/logout' }).then(() => {
      removeUserFromStorage();
      history.push('/');
      dispatch(userLogoutSuccess());
    });
  };
}
